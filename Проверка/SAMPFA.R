rm(list = ls())
library (quantmod)
library (PerformanceAnalytics)

alpha <- function(a, b, c) { 
  return (CAPM.alpha(a[,1,drop=FALSE], b[,1,drop=FALSE], c)) 
}
beta <- function(a, b, c) { 
  return (CAPM.beta(a[,1,drop=FALSE], b[,1,drop=FALSE], c)) 
}
treynorRatio <- function(a, b, c) { 
  return (TreynorRatio(a[,1,drop=FALSE], b[,1,drop=FALSE], c, scale = 252, modified = TRUE))
}
trackingError <- function(a, b) { 
  return(TrackingError(a[,1,drop=FALSE], b[,1,drop=FALSE], scale = 252)) 
}
jensesAlpha <- function(a, b, c) { 
  return (CAPM.jensenAlpha(a[,1,drop=FALSE], b[,1,drop=FALSE], c)) 
}
sharpRatio <- function(a, c) { 
  return(SharpeRatio(a[,1,drop=FALSE], c))
}

write <- function() {
  tic<-readline("Write a ticket ")
  x<-getSymbols(tic, src='yahoo',from = "2000-01-01", 
                to = Sys.Date(), auto.assign=FALSE )
  return(x) 
}


a<-write()
x<-getSymbols(Symbols = 'GOOGL', scr = "yahoo", from = "2014-01-01", to = "2015-01-01", auto.assign=FALSE)
a <- Return.calculate(prices = x[, 4, drop = FALSE])
a <- round(a,6)
getSymbols.yahoo(Symbols = "^GSPC", env = .GlobalEnv, return.class = "xts",  from = "2014-01-01", to = "2015-01-01")
b <- Return.calculate(prices = GSPC[, 4, , drop = FALSE])
b <- round(b,6)
risk_free_rate <- as.numeric(scan(what = "integer",nmax = 1))
beta(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate)
CAPM.alpha(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate)


TreynorRatio(round(Return.calculate(getSymbols.yahoo(Symbols = "ADBE", env = .GlobalEnv, return.class = "xts",  from = "2013-01-01", to = "2014-01-01"))[, 4, drop = FALSE],6)),
             round(Return.calculate(getSymbols.yahoo(Symbols = "^NDX", env = .GlobalEnv, return.class = "xts",  from = "2013-01-01", to = "2014-01-01"))[, 4, drop = FALSE],6)),
             25, 252, TRUE)
               



TreynorRatio(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate, scale = 252, modified = TRUE)
TreynorRatio(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate, scale = 252, modified = TRUE)
TreynorRatio(Ra = a[,1,drop=FALSE], Rb = b[,1,drop=FALSE], risk_free_rate, scale = 12, modified = FALSE)
TreynorRatio(Ra = a[,1,drop=FALSE], Rb = b[,1,drop=FALSE], risk_free_rate, scale = 4, modified = FALSE)


TreynorRatio()


x<-alpha(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate)
beta(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate)
jensesAlpha(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate)
trackingError(a[,1,drop=FALSE], b[,1,drop=FALSE])
treynorRatio(a[,1,drop=FALSE], b[,1,drop=FALSE], risk_free_rate)
sharpRatio(a[,1,drop=FALSE], risk_free_rate)



