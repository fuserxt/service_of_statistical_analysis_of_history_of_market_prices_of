import ystockquote
import xlwt
from datetime import datetime
#check for duplicates
def CheckingForDuplicatesC():
    input = open('Companies.txt')
    linesarray = input.readlines()
    seen = []
    for line in linesarray:
        if line not in seen:
            seen.append(line)
    input.close()
    input = open('Companies.txt', 'w')
    input.writelines(seen)

#check for duplicates
def CheckingForDuplicatesB():
    input = open('Benchmark.txt')
    linesarray = input.readlines()
    seen = []
    for line in linesarray:
        if line not in seen:
            seen.append(line)
    input.close()
    input = open('Benchmark.txt', 'w')
    input.writelines(seen)

 #delete empty lines from file with companies

#delete empty lines from file with companies
def DeletingRowsC():
    file = open('Companies.txt')
    seen=[]
    for line in file:
        if (len(line) == 1):
            del line
        else:
            seen.append(line)
    file.close()
    f = open('Companies.txt', 'w')
    f.writelines(seen)
    f.close()

#delete empty lines from file with indexes
def DeletingRowsB():
    file = open('Benchmark.txt')
    seen=[]
    for line in file:
        if (len(line) == 1):
            del line
        else:
            seen.append(line)
    file.close()
    f = open('Benchmark.txt', 'w')
    f.writelines(seen)
    f.close()

#print all companies in Companies.txt
def PrintCompanies():
    F = open('Companies.txt').readlines()
    fl = False
    for i in F:
        if not i.isspace():
            print(i.replace('\n', ''))
            fl = True
    if fl == False:
        print("!!!file is empty!!!")

#delete one company in Companies.txt
def DeleteCompany(delete_company):
    Company_txt = open('Companies.txt')
    Company = list()
    for line in Company_txt:
        Company.append(line)
    Company_txt.close()
    Company_txt = open('Companies.txt', 'w')
    begin_file = True
    for line in Company:
        if line.find(delete_company) == -1:
            if begin_file:
                Company_txt.write(line)
                begin_file = False
            else:
                Company_txt.write('\n' + line)
    Company_txt.close()

#add company and company's ticket to Companies.txt
def AddCompanyAndTicket(company_name, company_ticket):
    File = open('Companies.txt', 'a+')
    File.write(company_name + ': ' + company_ticket + "\n")
    File.close()
    CheckingForDuplicatesC()
    DeletingRowsC()

#get company's ticket from Companies.txt
def GetTicket(company_name):
    File = open('Companies.txt')
    for line in File:
        if line.find(company_name) != -1:
            fl = 0
            while line[fl] != ':':
                fl += 1
            return line[fl + 1 : len(line)-1].rstrip().lstrip()
            File.close()
    return False

#get benchmark's ticket from Benchmark.txt
def GetBenchmark(benchmark_in):
    File = open('Benchmark.txt')
    for line in File:
        if line.find(benchmark_in) != -1:
            fl = 0
            while line[fl] != ':':
                fl += 1
            return line[fl + 1 : len(line)].rstrip().lstrip()
            File.close()
    return False

#print all benchmarks from Benchmark.txt
def PrintBenchmark():
    F = open('Benchmark.txt').readlines()
    fl = False
    for i in F:
        if not i.isspace():
            print(i.replace('\n', ''))
            fl = True
    if fl == False:
        print("!!!file is empty!!!")

#add benchmark name and ticket to Benchmark.txt
def AddBenchmark(benchmark_name, benchmark_ticket):
    File = open('Benchmark.txt', 'a+')
    File.write(benchmark_name + ': ' + benchmark_ticket + "\n")
    File.close()
    CheckingForDuplicatesB()
    DeletingRowsB()

#delete one benchamrak from Benchmark.txt
def DeleteIndex(delete_index):
    File = open('Benchmark.txt')
    companies = list()
    for line in File:
        companies.append(line)
    File.close()
    File = open('Benchmark.txt', 'w')
    begin_file = True
    for line in companies:
        if line.find(delete_index) == -1:
            if begin_file == True:
                File.write(line)
                begin_file = False
            else:
                File.write('\n' + line)

#import history of data from finance.yahoo
def DataHistory(company_ticket, start, end):
    return ystockquote.get_historical_prices(company_ticket, start, end)

#calculation of company's yields
#      V(t) - V(t-1)
# Rr = ------------
#        V(t-1)
def TicketYield(ticket_data):
    ticket_data_list = list()
    ticket_dates_list = list(ticket_data)
    ticket_dates_list.sort()
    for i in range(len(ticket_data) - 1):
        pr = float(ticket_data[ticket_dates_list[i + 1]]['Close']) - float(ticket_data[ticket_dates_list[i]]['Close'])
        ticket_data_list.insert(len(ticket_data_list), (pr / float(ticket_data[ticket_dates_list[i]]['Close']))*100)
    return ticket_data_list

#check for missing dates
def GetBenchmarkClose(ticket_data, benchmark, start, end):
    benchmark_close = list()
    ticket_data_list = list(ticket_data)
    ticket_data_list.sort()
    benchmark_close_data = DataHistory(benchmark, start, end)
    benchmark_close_dates = list(benchmark_close_data)
    benchmark_close_dates.sort()
    for i in benchmark_close_dates:
        if i in ticket_data_list:
            benchmark_close.append(float(benchmark_close_data[i]['Close']))
    return benchmark_close

#calculation of benchmark's yield
#      V(t) - V(t-1)
# Rr = ------------
#        V(t-1)
def BenchmarkYield(benchmark_close):
    yields = list()
    for i in range(len(benchmark_close) - 1):
        yields.append(((benchmark_close[i + 1] - benchmark_close[i]) / (benchmark_close[i]))*100)
    return yields

#calculation of expected value
#     E(i=1..n)[Xi]
# q = ------------
#          n
def EValue(val):
    return sum(val, 0)/len(val)

#calculation of dispersion
#       E(i=1..n)[Xi - q]
# q^2 = -----------------
#             n
def Dispersion(val):
    disp = 0
    for i in range(len(val)):
        disp += (val[i] - EValue(val)) ** 2
    disp /= len(val)
    return (disp)

#        N*E(i=1..N)[Rmi*Ri] - E(i=1..N)[Rmi]*E(i=1..N)[Ri]
# Beta = ----------------------------------------------------
#             N*E(i=1..N)[Rmi]^2 - (E(i=1..N)[Rmi])^2
#
# Rm -- yield of benchmark
# R -- yield of ticket
def Beta(indx, tct):
    num = len(tct)
    sum_Rm_Ra = 0
    for i in range(num):
        sum_Rm_Ra += indx[i] * tct[i]
    sum_Rm = sum(indx, 0)
    sum_Ra = sum(tct, 0)
    sum_Rm2 = 0
    for i in range(num):
        sum_Rm2 += indx[i] ** 2
    beta = (num * sum_Rm_Ra - sum_Rm * sum_Ra) / (num * sum_Rm2 - (sum_Rm ** 2))
    return beta

#         E(i=1..N)[Ri] - beta*E(i=1..N)[Rmi]
# Alpha = -----------------------------------
#                         N
# Rm -- yield of benchmark
# R -- yield of ticket
def Alpha(beta, tct, indx):
    num = len(tct)
    sum_Ra = sum(tct, 0)
    sum_Rm = sum(indx, 0)
    alpha = (sum_Ra - beta * sum_Rm)/num
    return alpha

#                 M[R-Rf]
# SharpRatio = -------------
#               (D[R]^(1/2))
# R -- yield of ticket
# Rf -- risk free rate
# M[] -- expected value
# D[] -- dispersion
def Sharp(risk_free_rate, yield_of_ticket):
    disp = Dispersion(yield_of_ticket)
    yield_of_ticket_c = list()
    for i in yield_of_ticket:
        pr = i
        pr -= risk_free_rate
        yield_of_ticket_c.append(pr)
    return EValue(yield_of_ticket_c)/(disp ** (0.5))

#                M[R]-Rf
# TreynorRatio = -------
#                 beta
# R -- yield of ticket
# Rf -- risk free rate
# M[] -- expected value
def Treynor(risk_free_rate, yield_of_ticket, beta):
    #pprint(yield_of_ticket)
    yield_of_ticket_c = list()
    for i in yield_of_ticket:
        pr = i
        yield_of_ticket_c.append(pr-risk_free_rate)
    treynor_ratio = EValue(yield_of_ticket_c)/beta
    return treynor_ratio

# JensesAlpha = M[R] - r - beta*(M[Rm] - r)
# Rm -- yield of benchmark
# R -- yield of ticket
# r -- risk free rate
# M[] -- expected value
def JensesAlpha(yield_of_ticket, yield_of_benchmark, risk_free_rate, beta):
    jenses_alpha = EValue(yield_of_ticket) - risk_free_rate - beta*(EValue(yield_of_benchmark) - risk_free_rate)
    return jenses_alpha

# TrackingError = (D[R - Rm])^(1/2)
# D[] -- dispersion
# Rm -- yield of benchmark
# R -- yield of ticket
def TrackingError(yield_of_ticket, yield_of_benchmark):
    mas = []
    for i in range(len(yield_of_ticket)):
        yield_of_ticket[i] -= yield_of_benchmark[i]
        mas.append(yield_of_ticket[i])
    trackingerror = Dispersion(mas) ** (0.5)
    return trackingerror

#                    M[R] - M[Rm]
# InformationRatio = ------------
#                        sig
# M[] -- expected value
# Rm -- yield of benchmark
# R -- yield of ticket
# sig - sigma
def InformationRatio(risk_free_rate, beta, yield_of_ticket, yield_of_benchmark):
    jensesalpha = JensesAlpha(yield_of_ticket, yield_of_benchmark, risk_free_rate, beta)
    yield_of_ticket_c = list()
    for i in yield_of_ticket:
        yield_of_ticket_c.append(i)
    sig = TrackingError(yield_of_ticket_c, yield_of_benchmark)
    informationratio = jensesalpha/sig
    return informationratio

def GARCH(yield_of_ticket, length, A, B, Y):
    if (length == 0):
        return Y * Dispersion(yield_of_ticket) + A * (float(yield_of_ticket[length - 1]) ** 2)
    else:
        return Y * Dispersion(yield_of_ticket) + A * (float(yield_of_ticket[length - 1]) ** 2) + B * GARCH(yield_of_ticket, length - 1, A, B, Y)

def WriteExcel(name_company, beta, sharp, treynor, garch,  jenses_alpha, tracking_error, information_ratio):
    font0 = xlwt.Font()
    font0.name = 'Times New Roman'
    font0.colour_index = 2
    font0.bold = True

    style0 = xlwt.XFStyle()
    style0.font = font0

    style1 = xlwt.XFStyle()
    style1.num_format_str = 'D-MMM-YY'

    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')

    ws.write(1, 0, datetime.now(), style1)
    ws.write(1, 1, company_name, style1)
    ws.write(2, 0, 'Beta = ' + str(beta))
    ws.write(3, 0, 'Sharp = ' + str(jenses_alpha))
    ws.write(4, 0, 'Treynor = ' + str(treynor))
    ws.write(5, 0, 'Alpha = ' + str(sharp))
    ws.write(6, 0, 'Tracking Error = ' + str(information_ratio))
    ws.write(7, 0, 'Information Ratio = ' + str(tracking_error))
    ws.write(8, 0, 'GARCH = ' + str(garch))

    wb.save(name_company + '.xls')

def PrintCompanyIndicator(company_name, company_ticket, benchmark, start, end, risk_free_rate):
    #data of historical prices of ticket (open, close..)
    ticket_data_inf = DataHistory(company_ticket, start, end)

    benchmark_close = GetBenchmarkClose(ticket_data_inf, benchmark, start, end)

    yield_of_benchmark = BenchmarkYield(benchmark_close)

    yield_of_ticket = TicketYield(ticket_data_inf)

    com =raw_input('choose indicator:'
                   '\n1. beta'
                   '\n2. alpha'
                   '\n3. sharp'
                   '\n4. treynor'
                   '\n5. tracking_error'
                   '\n6. information_ratio'
                   '\n7. jenses_alpha'
                   '\n8. GARCH'
                   '\n9. print all indicators'
                   '\n10. save all indicators to excel file'
                   '\n--> ').lstrip().rstrip()
    if com == '1':
        beta = Beta(yield_of_benchmark, yield_of_ticket)
        print ('for '+ company_name +' beta: ' + str(beta))

    elif com == '2':
         beta = Beta(yield_of_benchmark, yield_of_ticket)
         alpha = JensesAlpha(beta, yield_of_ticket, yield_of_benchmark)
         print('for '+ company_name +' alphaMY: ' + str(alpha))

    elif com == '3':
         sharp = Sharp(risk_free_rate, yield_of_ticket)
         print('for '+ company_name +' sharp: ' + str(sharp))

    elif com == '4':
         beta = Beta(yield_of_benchmark, yield_of_ticket)
         treynor = Treynor(risk_free_rate, yield_of_ticket, beta)
         print('for '+ company_name +' treynor: ' + str(treynor))

    elif com == '5':
         tracking_error = TrackingError(yield_of_ticket, yield_of_benchmark)
         print('for '+ company_name +' tracking_error: ' + str(tracking_error))

    elif com == '6':
         beta = Beta(yield_of_benchmark, yield_of_ticket)
         information_ratio = InformationRatio(risk_free_rate, beta, yield_of_ticket, yield_of_benchmark)
         print('for '+ company_name +' information_ratio: ' + str(information_ratio))

    elif com == '7':
         beta = Beta(yield_of_benchmark, yield_of_ticket)
         jenses_alpha = JensesAlpha(yield_of_ticket, yield_of_benchmark, risk_free_rate, beta)
         print('for '+ company_name +' jenses alpha: ' + str(jenses_alpha))

    elif com == '8':
        length = len(yield_of_ticket)
        A = float(raw_input('enter A: ').lstrip().rstrip())
        B = float(raw_input('enter B: ').lstrip().rstrip())
        Y = float(raw_input('enter Y: ').lstrip().rstrip())
        garch = GARCH(yield_of_ticket, length, A, B, Y)
        print('for '+ company_name +' GARCH: ' + str(garch))

    elif com == '9':
        beta = Beta(yield_of_benchmark, yield_of_ticket)
        print ('for '+ company_name +' beta: ' + str(beta))
        sharp = Sharp(risk_free_rate, yield_of_ticket)
        print('for '+ company_name +' sharp: ' + str(sharp))
        treynor = Treynor(risk_free_rate, yield_of_ticket, beta)
        print('for '+ company_name +' treynor: ' + str(treynor))
        tracking_error = TrackingError(yield_of_ticket, yield_of_benchmark)
        print('for '+ company_name +' tracking_error: ' + str(tracking_error))
        information_ratio = InformationRatio(risk_free_rate, beta, yield_of_ticket, yield_of_benchmark)
        print('for '+ company_name +' information_ratio: ' + str(information_ratio))
        jenses_alpha = JensesAlpha(yield_of_ticket, yield_of_benchmark, risk_free_rate, beta)
        print('for '+ company_name +' jenses alpha: ' + str(jenses_alpha))

    elif com == '10':
        beta = Beta(yield_of_benchmark, yield_of_ticket)
        sharp = Sharp(risk_free_rate, yield_of_ticket)
        treynor = Treynor(risk_free_rate, yield_of_ticket, beta)
        tracking_error = TrackingError(yield_of_ticket, yield_of_benchmark)
        information_ratio = InformationRatio(risk_free_rate, beta, yield_of_ticket, yield_of_benchmark)
        jenses_alpha = JensesAlpha(yield_of_ticket, yield_of_benchmark, risk_free_rate, beta)

        length = len(yield_of_ticket)
        A = float(raw_input('enter A: ').lstrip().rstrip())
        B = float(raw_input('enter B: ').lstrip().rstrip())
        Y = float(raw_input('enter Y: ').lstrip().rstrip())
        garch = GARCH(yield_of_ticket, length, A, B, Y)

        WriteExcel(company_name, beta, sharp, treynor, garch,
                   jenses_alpha, tracking_error, information_ratio)

command = True
while command != '0':
    command = raw_input("\nENTER THE COMMAND: "
                        "\n1. print companies"
                        "\n2. print company ticket"
                        "\n3. print benchmark"
                        "\n4. print benchmark ticket"
                        "\n5. add company"
                        "\n6. add benchmark"
                        "\n7. delete company"
                        "\n8. delete companies"
                        "\n9. delete benchmark"
                        "\n10. delete benchmarks"
                        "\n11. company indicator"
                        "\n12. company indicators"
                        "\n--> ").lstrip().rstrip()

    if command == '11':
        print('List of companies:')
        PrintCompanies()
        company_name = raw_input("\nCompany's name: ").lstrip().rstrip()
        company_ticket = GetTicket(company_name)
        if company_ticket == False:
            print('!!!company is not exist in list!!!')
        else:
            print('List of benchmark:')
            PrintBenchmark()
            benchmark_in = raw_input('\nChoose benchmark: ').lstrip().rstrip()
            benchmark = GetBenchmark(benchmark_in)
            if benchmark == False:
                print('\n!!!index is not exist in list!!!')
            else:
                start = raw_input("Data of begin: ")
                end = raw_input("Data of end: ")
                risk_free_rate = float(raw_input("Risk free rate: "))
                PrintCompanyIndicator(company_name, company_ticket, benchmark, start, end, risk_free_rate)

    elif command == '1':
        DeletingRowsC()
        print('List of companies:')
        PrintCompanies()

    elif command == '2':
        print('List of companies:')
        PrintCompanies()
        company_name = raw_input("\nCompany's name: ").lstrip().rstrip()
        rez = GetTicket(company_name)
        if rez != False:
            print(rez)
        else:
            print('!!!Company is not exist in list!!!')

    elif command == '3':
        DeletingRowsB()
        print('List of benchmarks:')
        PrintBenchmark()

    elif command == '4':
        print('List of benchmarks:')
        PrintBenchmark()
        benchmark_name = raw_input("\nBenchmark's name: ").lstrip().rstrip()
        rez = GetBenchmark(benchmark_name)
        if rez != False:
            print(rez)
        else:
            print('!!!Benchmark is not exist in list!!!')

    elif command == '5':
        company_name = raw_input("Company's name: ").lstrip().rstrip()
        company_ticket = raw_input("Company's ticket: ").lstrip().rstrip()
        AddCompanyAndTicket(company_name, company_ticket)

    elif command == '6':
        benchmark_name = raw_input('Name benchmark: ')
        benchmark_symbol= raw_input("Benchmark's ticket: ")
        AddBenchmark(benchmark_name, benchmark_symbol)

    elif command == '7':
        print('List of companies:')
        PrintCompanies()
        name_company = raw_input('\nCompany name: ').lstrip().rstrip()
        DeleteCompany(name_company)
        DeletingRowsC()

    elif command == '8':
        print('List of companies:')
        PrintCompanies()
        input_l = []
        input_list = []
        input_l = raw_input('\nCompany names (write a comma): ').split(',')
        for i in input_l:
            i = i.lstrip().rstrip()
            input_list.append(i)
        del input_l
        for s in input_list:
            DeleteCompany(s)
        DeletingRowsC()

    elif command == '9':
        print('List of benchmarks:')
        PrintBenchmark()
        delete_index = raw_input('\nBenchmark name: ').lstrip().rstrip()
        DeleteIndex(delete_index)
        DeletingRowsB()

    elif command == '10':
        print('List of benchmarks:')
        PrintBenchmark()
        input_l = []
        input_list = []
        input_l = raw_input('\nBenchmark names (write a comma): ').split(',')
        for i in input_l:
            i = i.lstrip().rstrip()
            input_list.append(i)
        del input_l
        for s in input_list:
            DeleteIndex(s)
        DeletingRowsB()

    else:
        if command == '0':
            exit(0)
        else:
            print('///command not found///\n')
